﻿using System;
using Characters.Player.FlingBall;
using UnityEngine;

namespace Characters.Player
{
    public class PlayerSuperSonic : MonoBehaviour
    {
        private CharacterController2D _controller;
        [SerializeField] private int _superSonicDivider = 25;
        public int SuperSonicLevel { get; private set; } = 1;

        private void OnEnable()
        {
            _controller = GetComponent<CharacterController2D>();
        }

        private void Update()
        {
            // Check super sonic status
            SuperSonicLevel = (int)(_controller.CurrentSpeed / _superSonicDivider);

            // Update visuals if changed
        }
    }
}