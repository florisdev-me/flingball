using Characters.Player.FlingBall;
using ScriptableObjects;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.PlayerLoop;
using Utils;

namespace Characters.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        private CharacterController2D _controller;
        [SerializeField] private InputHandler _input;
        [SerializeField] private PlayerEventsSO _playerEvents;
        
        private void OnEnable() {
            _controller = GetComponent<CharacterController2D>();

            _input.MouseMoveEvent += OnMouseMove;
            _input.MouseHoldEvent += OnMouseHold;

            _playerEvents.RoomFinished += OnRoomFinished;
            _playerEvents.NewRoomLoaded += OnNewRoomLoaded;
        }
        
        private void OnDisable()
        {
            _input.MouseMoveEvent -= OnMouseMove;
            _input.MouseHoldEvent -= OnMouseHold;

            _playerEvents.RoomFinished -= OnRoomFinished;
        }

        private void OnRoomFinished()
        {
            _controller.ExitRoom();
        }
        
        private void OnNewRoomLoaded(Vector3 spawnPos)
        {
            _controller.EnterRoom();
            transform.position = spawnPos;
        }

        private void OnMouseMove(Vector2 position)
        {
            // _controller
            var cursorPos = MouseMath.MouseWorldPosition2D(position);
            _controller.SetCursorPos(cursorPos);
        }

        private void OnMouseHold(bool held)
        {
            if (Touchscreen.current is { wasUpdatedThisFrame: true })
            {
                var cursorPos = MouseMath.MouseWorldPosition2D(Touchscreen.current.primaryTouch.position.ReadValue());
                _controller.SetCursorPos(cursorPos);
            }
            
            if (held)
            {
                _controller.MouseDown();
            }
            else
            {
                _controller.MouseUp();
            }
        }

    }
}
