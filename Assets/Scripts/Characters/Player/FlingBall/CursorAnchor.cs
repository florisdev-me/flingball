using UnityEngine;

namespace Characters.Player.FlingBall
{
    public class CursorAnchor : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _renderer;

        public bool Hidden { get; private set; } = true;

        private void Update()
        {
            _renderer.enabled = !Hidden;
        }

        public void Hide()
        {
            Hidden = true;
        }

        public void Show()
        {
            Hidden = false;
        }

        public void UpdatePosition(Vector2 position)
        {
            transform.position = position;
        }
    }
}
