using System;
using GameSetup;
using UnityEngine;

namespace Characters.Player.FlingBall
{
    public class CharacterController2D : MonoBehaviour
    {
        private Rigidbody2D _rigidbody;

        [Header("Movement properties")] [SerializeField]
        private float _maxSpeedWhileHeld = 1;

        [SerializeField] private float _maxSpeed = 1;
        [SerializeField] private float _deceleration = .025f;
        [SerializeField] private float _rotationSpeed = 10;
        [SerializeField] private float _flingPower = 10f;

        [Header("Visual Feedback")] [SerializeField]
        private CursorAnchor _anchor;

        private Vector2 _direction = Vector2.zero;

        public float CurrentSpeed { get; private set; } = 0.1f;

        // Mouse variables
        private bool _mouseHeld;
        private Vector2 _mousePos;
        private Vector2 _mouseDelta;
        private Vector2 _prevMousePos;

        // Movement offset
        private Vector2 _mouseOffset;

        // Status variables, serialized for debugging purposes
        [Tooltip("Leave at false, except while debugging level")]
        [SerializeField] private bool _simulating = false;

        private void OnEnable()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        public void SetCursorPos(Vector2 position)
        {
            _mousePos = position;
            _anchor.UpdatePosition(_mousePos + _mouseOffset);
        }

        private void FixedUpdate()
        {
            if (!_simulating)
            {
                return;
            }

            _mouseDelta = _mousePos - _prevMousePos;
            _prevMousePos = _mousePos;

            if (!_mouseHeld)
            {
                // Normal deceleration, while floating
                CurrentSpeed *= 1 - _deceleration;
                _rigidbody.velocity = _direction * CurrentSpeed;
                return;
            }

            var targetPos = _mousePos + _mouseOffset;
            _direction = Vector2.Lerp(_direction, targetPos - _rigidbody.position, _rotationSpeed);

            CurrentSpeed += _direction.magnitude;
            CurrentSpeed = Math.Min(_maxSpeedWhileHeld, CurrentSpeed);

            _rigidbody.velocity = _direction.normalized * (CurrentSpeed * Math.Min(1, _direction.magnitude));
        }

        public void MouseUp()
        {
            if (!_simulating)
            {
                return;
            }
            
            _mouseHeld = false;
            Fling();
            _anchor.Hide();
        }

        private void Fling()
        {
            // Launch when releasing
            var targetPos = _mousePos + _mouseOffset;
            _direction = Vector2.Lerp(_direction, targetPos - _rigidbody.position, _rotationSpeed * 2);

            CurrentSpeed += _mouseDelta.magnitude * _flingPower;
            CurrentSpeed = Math.Min(_maxSpeed, CurrentSpeed);

            _rigidbody.velocity = _direction * CurrentSpeed;
        }

        public void MouseDown()
        {
            if (!_simulating)
            {
                return;
            }
            
            _mouseHeld = true;
            _mouseOffset = _rigidbody.position - _mousePos;
            _anchor.UpdatePosition(_mousePos + _mouseOffset);

            if (_anchor != null)
            {
                _anchor.Show();
            }
        }

        public void Bounce(Collision2D collision)
        {
            if (_mouseHeld || collision.contactCount < 1)
            {
                return;
            }

            var firstContact = collision.contacts[0];

            // R = 𝐷 − 2(𝐷 ∙ 𝑁)N
            var reflected = _direction - 2 * Vector2.Dot(_direction, firstContact.normal) * firstContact.normal;

            _direction = reflected;
            CurrentSpeed *= .75f;
        }

        public void ReduceSpeed()
        {
            CurrentSpeed *= .5f;
        }

        public void ExitRoom()
        {
            _simulating = false;
            _anchor.Hide();
            _rigidbody.velocity = Vector2.zero;
            CurrentSpeed = 0;
            _direction = Vector2.zero;
        }

        public void EnterRoom()
        {
            _simulating = true;
        }
    }
}