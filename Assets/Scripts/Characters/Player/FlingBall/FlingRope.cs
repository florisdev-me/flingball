using UnityEngine;

namespace Characters.Player.FlingBall
{
    public class FlingRope : MonoBehaviour
    {
        private LineRenderer _line;

        [SerializeField] private CursorAnchor _anchor;
        [SerializeField] private Transform _ball;
    
        private void OnEnable()
        {
            _line = GetComponent<LineRenderer>();
            _line.positionCount = 2;
        }

        private void Update()
        {
            _line.enabled = !_anchor.Hidden;

            if (!_line.enabled)
            {
                return;
            }
        
            _line.SetPosition(0, _anchor.transform.position);
            _line.SetPosition(1, _ball.position);
        }
    }
}
