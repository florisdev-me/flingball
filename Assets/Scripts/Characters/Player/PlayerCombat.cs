using System;
using System.Linq;
using Characters.Player.FlingBall;
using Environment;
using ScriptableObjects;
using UnityEngine;

namespace Characters.Player
{
    public enum CollisionResult
    {
        None,
        EnemyDestroyed
    }

    public class PlayerCombat : MonoBehaviour
    {
        private CharacterController2D _controller;
        private PlayerSuperSonic _superSonic;

        [SerializeField] private PlayerLifesSO _playerLifes;
        [SerializeField] private GameManagerSO _gameManager;

        [SerializeField] private ParticleSystem _damageEffect;
        [SerializeField] private GameObject _collisionEffect;

        private void OnEnable()
        {
            _controller = GetComponent<CharacterController2D>();
            _superSonic = GetComponent<PlayerSuperSonic>();
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            var collisionResult = CollisionResult.None;
            if (collision.gameObject.CompareTag("Hostile"))
            {
                var hostile = collision.gameObject.GetComponent<HostileComponent>();
                collisionResult = hostile.HandleCollision(this, _superSonic);

                if (collisionResult == CollisionResult.EnemyDestroyed)
                {
                    hostile.Destroy();
                }
            }

            var contact = collision.contacts.First();

            switch (collisionResult)
            {
                case CollisionResult.None:
                    var rotation = Quaternion.FromToRotation(Vector3.right, contact.normal);
                    var effect = Instantiate(_collisionEffect, contact.point, rotation);
                    effect.transform.parent = _gameManager.CurrentRoom.transform;
                    
                    _controller.Bounce(collision);
                    
                    break;
                case CollisionResult.EnemyDestroyed:
                    _controller.ReduceSpeed();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void DoDamage()
        {
            _playerLifes.TakeHit();

            // Hit Effect
            _damageEffect.Play();
        }
    }
}