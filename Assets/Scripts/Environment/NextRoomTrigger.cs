using System;
using ScriptableObjects;
using UnityEngine;

namespace Environment
{
    public class NextRoomTrigger : MonoBehaviour
    {
        [SerializeField] private GameManagerSO _gameManager;
        
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!other.gameObject.CompareTag("Player"))
                return;
            
            _gameManager.CompletedRoom();
        }
    }
}
