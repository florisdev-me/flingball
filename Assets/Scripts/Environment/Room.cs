using UnityEngine;

namespace Environment
{
    public class Room : MonoBehaviour
    {
        [SerializeField] private Transform _spawnPoint;
        public Transform SpawnPoint => _spawnPoint;

        public void ActivateRoom()
        {
            transform.position = Vector3.zero;
        }

        public void DeactivateRoom()
        {
            Destroy(gameObject);
        }
    }
}
