using System;
using Characters.Player;
using UnityEngine;

namespace Environment
{
    /// <summary>
    /// Anything hostile that hurts the player on contact
    /// </summary>
    public class HostileComponent : MonoBehaviour
    {
        [SerializeField]
        private Hostile _parent;
        
        public CollisionResult HandleCollision(PlayerCombat combat, PlayerSuperSonic superSonic)
        {
            return _parent.HandleCollision(combat, superSonic);
        }

        public void Destroy()
        {
            _parent.Destroy();
        }
    }
}
