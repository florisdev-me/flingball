﻿using Characters.Player;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Environment
{
    public class Hostile : MonoBehaviour
    {
        [SerializeField] private int _superSonicThreshold = 1;
        
        public CollisionResult HandleCollision(PlayerCombat combat, PlayerSuperSonic superSonic)
        {
            if (superSonic.SuperSonicLevel > _superSonicThreshold) 
                return CollisionResult.EnemyDestroyed;
            
            combat.DoDamage();
            return CollisionResult.None;

        }

        public void Destroy()
        {
            Destroy(this.gameObject);
        }
    }
}