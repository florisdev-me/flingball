using System;
using System.Collections.Generic;
using ScriptableObjects;
using UnityEngine;

namespace CustomGUI
{
    public class ToggleCanvasOnGameState : MonoBehaviour
    {
        [SerializeField] private List<GameState> _activeGameStates;
        [SerializeField] private GameManagerSO _gameManager;

        private CanvasGroup _canvasGroup;
        private bool _shouldInteract;
        private bool _blockRayCasts;

        private void OnEnable()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
            _shouldInteract = _canvasGroup.interactable;
            _blockRayCasts = _canvasGroup.blocksRaycasts;
            
            _gameManager.GameStateManager.GameStateChangeEvent += Toggle;
            
            Toggle(_gameManager.GameStateManager.CurrentGameState);
        }

        private void OnDisable()
        {
            _canvasGroup.alpha = 1;
            _canvasGroup.interactable = _shouldInteract;
            _canvasGroup.blocksRaycasts = _blockRayCasts;
            
            _gameManager.GameStateManager.GameStateChangeEvent -= Toggle;
        }

        private void Toggle(GameState newState)
        {
            if (_activeGameStates.Contains(newState))
            {
                _canvasGroup.alpha = 1;
                _canvasGroup.interactable = _shouldInteract;
                _canvasGroup.blocksRaycasts = _blockRayCasts;
            }
            else
            {
                _canvasGroup.alpha = 0;
                _canvasGroup.interactable = false;
                _canvasGroup.blocksRaycasts = false;
            }
        }
    }
}
