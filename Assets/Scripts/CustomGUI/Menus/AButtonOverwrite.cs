using System;
using UnityEngine;
using UnityEngine.UI;

namespace CustomGUI.Menus
{
    public abstract class AButtonOverwrite : MonoBehaviour
    {
        protected Button button;
        
        private void OnEnable()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(OnClick);
        }

        protected abstract void OnClick();
    }
}
