using System;
using System.Collections.Generic;
using System.Linq;
using ScriptableObjects;
using UnityEngine;

namespace CustomGUI
{
    public class LifesDisplay : MonoBehaviour
    {
        [SerializeField] private GameObject _lifeIcon;
        [SerializeField] private PlayerLifesSO _playerLifes;

        private List<GameObject> _children;

        private void OnEnable()
        {
            _playerLifes.TakeHitEvent += OnTakeHit;

            _children = new List<GameObject>();

            for (var i = 0; i < _playerLifes.CurrentLifes; i++)
            {
                var lifeIcon = Instantiate(_lifeIcon, this.transform);
                _children.Add(lifeIcon);
            }
        }

        private void OnDisable()
        {
            _playerLifes.TakeHitEvent -= OnTakeHit;
        }

        private void OnTakeHit()
        {
            var lastLifeIcon = _children.Last();
            _children.Remove(lastLifeIcon);
            Destroy(lastLifeIcon);
        }
    }
}
