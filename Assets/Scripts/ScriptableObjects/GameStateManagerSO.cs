using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjects
{
    public enum GameState
    {
        Playing,
        Paused,
        LoadingLevel,
        GameOver,
        Victory
    }
    
    [CreateAssetMenu(fileName = "GameStateManager", menuName = "Managers/GameState Manager")]
    public class GameStateManagerSO : ScriptableObject
    {
        public event UnityAction<GameState> GameStateChangeEvent = delegate { };

        public GameState CurrentGameState { get; private set; } = GameState.Playing;
        
        public void SetGameState(GameState newGameState)
        {
            CurrentGameState = newGameState;
            GameStateChangeEvent.Invoke(CurrentGameState);
        }
    }
}
