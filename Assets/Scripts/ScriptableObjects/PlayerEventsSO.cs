using UnityEngine;
using UnityEngine.Events;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = "PlayerEvents", menuName = "Player/Player Events")]
    public class PlayerEventsSO : ScriptableObject
    {
        public UnityAction RoomFinished = delegate {  };
        public UnityAction<Vector3> NewRoomLoaded = delegate(Vector3 spawnPoint) {  }; 
    }
}
