using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace ScriptableObjects
{

    [CreateAssetMenu(fileName = "SceneData", menuName = "Game Setup/Scene Data")]
    public class SceneDataSO : ScriptableObject
    {
        [SerializeField] private string _sceneName;
        public bool LoadRooms = false;

        public IEnumerator LoadSceneAsync(Action afterLoad)
        {
            // Start loading the scene
            AsyncOperation asyncLoadLevel = SceneManager.LoadSceneAsync(_sceneName, LoadSceneMode.Single);
            
            // Wait until the level finish loading
            while (!asyncLoadLevel.isDone)
                yield return null;
            
            // Wait a frame so every Awake and Start method is called
            yield return new WaitForEndOfFrame();

            afterLoad();
        }
    }
}