using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = "InputHandler", menuName = "Game Setup/Input Handler")]
    public class InputHandler : ScriptableObject, GameInput.IGameplayActions
    {
        
        public event UnityAction<bool> MouseHoldEvent = delegate { };
        public event UnityAction<Vector2> MouseMoveEvent = delegate { };
        
        private GameInput _gameInput;

        private void OnEnable()
        {
            if (_gameInput == null)
            {
                _gameInput = new GameInput();
                _gameInput.Gameplay.SetCallbacks(this);
            }

            // Enable desire input scheme
            _gameInput.Gameplay.Enable();
        }

        private void OnDisable()
        {
            // Disable all input schemes
            _gameInput.Gameplay.Disable();
        }

        public void OnMouseMove(InputAction.CallbackContext context)
        {
            MouseMoveEvent.Invoke(context.ReadValue<Vector2>());
        }

        public void OnMouseHold(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                MouseHoldEvent.Invoke(true);
            }
            else if (context.canceled)
            {
                MouseHoldEvent.Invoke(false);
            }
        }
    }
}
