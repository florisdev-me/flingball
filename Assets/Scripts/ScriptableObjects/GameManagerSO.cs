using System.Collections.Generic;
using Environment;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Utils;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = "GameManager", menuName = "Managers/Game Manager")]
    public class GameManagerSO : ScriptableObject
    {
        [SerializeField] private PlayerLifesSO _playerLifes;
        [SerializeField] private PlayerEventsSO _playerEvents;

        public GameStateManagerSO GameStateManager;

        // TODO: Replace with some sort of room manager/spawner
        [SerializeField] private GameObject _roomPrototype;

        public Room CurrentRoom { get; private set; }
        private Room _nextRoom;

        public void GameOver()
        {
            GameStateManager.SetGameState(GameState.GameOver);
        }

        public void StartLevel(SceneDataSO sceneData)
        {
            _playerLifes.Reset();

            UnityHelper.Instance.StartCoroutine(sceneData.LoadSceneAsync(() => OnSceneLoaded(sceneData.LoadRooms)));
        }

        private void OnSceneLoaded(bool loadRooms = false)
        {
            GameStateManager.SetGameState(GameState.Playing);

            if (!loadRooms) return;

            CurrentRoom = Instantiate(_roomPrototype, Vector3.zero, Quaternion.identity).GetComponent<Room>();
            CurrentRoom.ActivateRoom();

            _nextRoom = Instantiate(_roomPrototype, new Vector3(0, 1000, 0), Quaternion.identity).GetComponent<Room>();
            CurrentRoom.gameObject.name = "CurrentRoom";
            _nextRoom.gameObject.name = "NextRoom";
            
            _playerEvents.NewRoomLoaded.Invoke(CurrentRoom.SpawnPoint.position);
        }

        public void CompletedRoom()
        {
            _playerEvents.RoomFinished.Invoke();

            // Goto next room
            CurrentRoom.DeactivateRoom();
            _nextRoom.ActivateRoom();
            
            _playerEvents.NewRoomLoaded.Invoke(_nextRoom.SpawnPoint.position);

            // Generate new room
            CurrentRoom = _nextRoom;
            _nextRoom = Instantiate(_roomPrototype, new Vector3(0, 1000, 0), Quaternion.identity).GetComponent<Room>();
            
            CurrentRoom.gameObject.name = "CurrentRoom";
            _nextRoom.gameObject.name = "NextRoom";
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}