﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.PlayerLoop;

namespace ScriptableObjects
{
    [CreateAssetMenu(fileName = "PlayerLifes", menuName = "Player/Player Lifes")]
    public class PlayerLifesSO : ScriptableObject
    {
        public int CurrentLifes { get; private set; }

        public UnityAction TakeHitEvent = delegate { };

        [SerializeField] private int _initialHealth;

        [SerializeField] private GameManagerSO _gameManager;

        public void Reset()
        {
            CurrentLifes = _initialHealth;
        }

        public void TakeHit()
        {
            CurrentLifes--;

            if (CurrentLifes <= 0)
            {
                _gameManager.GameOver();
            }
            else
            {
                TakeHitEvent.Invoke();
            }
        }
    }
}