using System;
using ScriptableObjects;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Utils;

namespace GameSetup
{
    public class WorldCursor : MonoBehaviour
    {
        [SerializeField] private InputHandler _input;
        
        private void OnEnable()
        {
            _input.MouseMoveEvent += OnMouseMove;
        }

        private void OnDisable()
        {
            _input.MouseMoveEvent -= OnMouseMove;
        }

        private void OnMouseMove(Vector2 pos)
        {
            // For debug purposes
            transform.position = MouseMath.MouseWorldPosition2D(pos);
        }
    }
}
