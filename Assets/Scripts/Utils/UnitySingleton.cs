﻿using UnityEngine;

namespace Utils
{
    [ExecuteInEditMode]
    public class UnitySingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance = null;
        
        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<T>();
                    if (_instance == null)
                    {
                        var singletonObject = new GameObject(typeof(T).ToString());
                        _instance = singletonObject.AddComponent<T>();
                    }
                }
                return _instance;
            }
        }

        protected virtual void Awake()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
                return;
            }
            _instance = GetComponent<T>();

            DontDestroyOnLoad(gameObject);
        }
    }
}