using UnityEngine;

namespace Utils
{
    /// <summary>
    /// Helper class for any class that requires Unity/Monobehaviour functionality, but has no direct access
    /// </summary>
    public class UnityHelper : UnitySingleton<UnityHelper>
    {
        
    }
}
