    using UnityEngine;

    namespace Utils
    {
        public static class MouseMath
        {
            public static Vector3 MouseWorldPosition(Vector2 screenPos, float height)
            {
                var plane = new Plane(Vector3.up, new Vector3(0, height, 0));

                var ray = Camera.main.ScreenPointToRay(screenPos);
                float dist = 0;
                
                return plane.Raycast(ray, out dist) ? ray.GetPoint(dist) : Vector3.one;
            }

            public static Vector2 MouseWorldPosition2D(Vector2 screenPos)
            {
                return Camera.main.ScreenToWorldPoint(screenPos);
            }
        }
    }